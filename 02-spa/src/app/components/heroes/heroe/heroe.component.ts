import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Heroe } from 'src/app/models/heroe';
import { HeroesService } from '../../../services/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent {

  heroe: Heroe;
  casa: string;

  constructor(private activatedRoute: ActivatedRoute, private heroesService: HeroesService) {
    this.activatedRoute.params.subscribe(params => this.heroe = heroesService.getHeroe(params['id']));
    if (this.heroe.casa === 'DC')
      this.casa = 'assets/img/dc-logo.jpg';
    if (this.heroe.casa === 'Marvel')
      this.casa = 'assets/img/marvel-logo.png';
  }



}
