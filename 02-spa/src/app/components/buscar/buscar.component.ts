import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';
import { Heroe } from '../../models/heroe';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html'
})
export class BuscarComponent implements OnInit {

  heroes:Heroe[] = [];

  constructor(private activatedRoute: ActivatedRoute, private heroesService: HeroesService) { }


  ngOnInit() : void{
    this.activatedRoute.params.subscribe(params => this.heroes = this.heroesService.buscarHeroes(params['buscar']));
  }

}
