import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  constructor(private http:HttpClient) { }

  private endpont:string = 'https://restcountries.eu/rest/v2/lang/es';
  getPaises(){
    return this.http.get(this.endpont)
      .pipe(
        map((response:any[]) => response.map(pais => ({ nombre: pais.name, codigo: pais.alpha3Code })))
      );
  }
}
