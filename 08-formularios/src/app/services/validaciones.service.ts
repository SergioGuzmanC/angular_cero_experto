import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

interface ErrorValidate{
  [s:string]:boolean;
}
@Injectable({
  providedIn: 'root'
})
export class ValidacionesService {

  constructor() { }

  noGuzman(control: FormControl): ErrorValidate {

    if (control.value ?.toLowerCase() === 'guzman') {
      return {
        noGuzman: true
      };
    }
    return null;
  }

  passwordIguales(pass1: string, pass2: string) {

    return (formGroup: FormGroup)=>{
      let p1 = formGroup.controls[pass1];
      let p2 = formGroup.controls[pass2];

      if(p1.value === p2.value) p2.setErrors(null);
      else p2.setErrors({ noEsIgual: true})
    }
  }

  existeUsuario(control:FormControl) : Promise<ErrorValidate> | Observable<ErrorValidate>{
    if(!control.value) return new Promise(null);
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
          if(control.value === 'papu') resolve ({existe:true});
          else resolve(null)
        }, 3500)
    });
  }

}
