import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ValidacionesService } from 'src/app/services/validaciones.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  forma: FormGroup;

  constructor(private formBuilder: FormBuilder, private validaciones: ValidacionesService) {
    this.crearFormulario();
    this.cargarDatos();
    this.crearListeners();
  }

  ngOnInit(): void {
  }

  get pasatiempos() {
    return this.forma.get('pasatiempos') as FormArray;
  }
  get nombreNoValido() {
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched;
  }
  get apellidoNoValido() {
    return this.forma.get('apellido').invalid && this.forma.get('apellido').touched;
  }
  get correoNoValido() {
    return this.forma.get('correo').invalid && this.forma.get('correo').touched;
  }
  get calleNoValido() {
    return this.forma.get('direccion.calle').invalid && this.forma.get('direccion.calle').touched;
  }
  get coloniaNoValido() {
    return this.forma.get('direccion.colonia').invalid && this.forma.get('direccion.colonia').touched;
  }
  get usuarioNoValido() {
    return this.forma.get('usuario').invalid && this.forma.get('usuario').touched;
  }
  get passwordNoValido() {
    return this.forma.get('pass').invalid && this.forma.get('pass').touched;
  }
  get confirmarNoValido() {
    let pass1 = this.forma.get('pass').value;
    let pass2 = this.forma.get('confirmar').value;
    return pass1 === pass2 ? false : true;
  }

  crearFormulario() {
    this.forma = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(4)]],
      apellido: ['', [Validators.required, Validators.minLength(4), this.validaciones.noGuzman]],
      correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      usuario: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)], [this.validaciones.existeUsuario]],
      pass: ['', [Validators.required]],
      confirmar: ['', [Validators.required]],
      direccion: this.formBuilder.group({
        calle: ['', [Validators.required, Validators.minLength(3)]],
        colonia: ['', [Validators.required, Validators.minLength(3)]]
      }),
      pasatiempos: this.formBuilder.array([
      ])
    }, {
        validators: [this.validaciones.passwordIguales('pass', 'confirmar')]
      });
  }


  guardar() {
    console.log(this.forma);
    console.log(this.forma.value);
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach(control => {
        if (control instanceof FormGroup)
          Object.values(control.controls).forEach(cont => cont.markAsTouched());
        else
          control.markAsTouched();
      });
    }
    this.forma.reset({
      nombre: 'Papu'
    });
  }

  private cargarDatos() {
    this.forma.reset(
      {
        nombre: 'Erik',
        apellido: 'Guzman2',
        pass: '123',
        confirmar: '123',
        correo: 'ercheco1@gmail.com',
        direccion: {
          calle: 'Rosita alvirez',
          colonia: 'Benito Juarez'
        }
      }
    );
  }

  agregarPasaTiempo() {
    this.pasatiempos.push(this.formBuilder.control('', [Validators.required]));
  }

  borrarPasaTiempo(id: number) {
    this.pasatiempos.removeAt(id);
  }

  crearListeners() {
    //    this.forma.valueChanges.subscribe(valor => console.log(valor));
    //    this.forma.statusChanges.subscribe(status => console.log(status));
    this.forma.get('nombre').valueChanges.subscribe(console.log);
  }
}
