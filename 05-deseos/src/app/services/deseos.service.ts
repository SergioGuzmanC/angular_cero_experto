import { Injectable, ɵSWITCH_CHANGE_DETECTOR_REF_FACTORY__POST_R3__ } from '@angular/core';
import { Lista } from '../models/Lista.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  lista: Lista[] = [];

  constructor() {
    this.cargarStorage();
  }

  crearLista(titulo: string): number {
    const nuevaLista = new Lista(titulo);
    this.lista.push(nuevaLista);
    this.guardarStorage();
    return nuevaLista.id;
  }

  boorarLista(lista: Lista) {
    this.lista = this.lista.filter(data => data.id !== lista.id);
    this.guardarStorage();
  }

  obtenerLista(id: number | string) {
    id = Number(id);
    return this.lista.find(listaData => listaData.id === id);
  }

  guardarStorage() {
    localStorage.setItem('data', JSON.stringify(this.lista));
  }

  cargarStorage() {
    if (localStorage.getItem('data')) {
      return this.lista = JSON.parse(localStorage.getItem('data'));
    }
  }
}
