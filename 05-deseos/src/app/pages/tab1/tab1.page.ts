import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private alertCtrl:AlertController, private deseosService:DeseosService, private router:Router) { }
  async agregarLista() {
    const alert = await this.alertCtrl.create({
      header: 'Nueva Lista',
      inputs: [{
        name: 'titulo',
        type: 'text',
        placeholder: 'Nombre de la lista'
      }
      ],
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },{
        text: 'Crear',
        handler: (data)=>{
          if(data.titulo.length === 0){
            return;
          }
          let listaId = this.deseosService.crearLista(data.titulo);          
        this.router.navigateByUrl(`/pendientes/agregar/${listaId}`);
        }
      }]
   });
    alert.present();
  }
}
