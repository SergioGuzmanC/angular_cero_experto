import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListItems } from 'src/app/models/list-item.model';
import { Lista } from 'src/app/models/Lista.model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {

  lista: Lista;
  nombreItem: string = '';

  constructor(private deseosService: DeseosService, private route: ActivatedRoute) {
    let listaId = this.route.snapshot.paramMap.get('listaId');
    this.lista = this.deseosService.obtenerLista(listaId);

  }

  ngOnInit() {
  }

  agregarItem() {
    if (this.nombreItem.length === 0) {
      return;
    }
    let nuevoItem = new ListItems(this.nombreItem);
    this.lista.items.push(nuevoItem);
    this.nombreItem = '';
    this.deseosService.guardarStorage();
  }

  cambioCheck(item: ListItems) {
    let pendientes = this.lista.items.filter(itemData => !itemData.terminado).length;
    if (pendientes === 0) {
      this.lista.termiandoEn = new Date();
      this.lista.terminado = true;
    } else {
      this.lista.termiandoEn = null;
      this.lista.terminado = false;

    }
    this.deseosService.guardarStorage();
  }

  borrar(index: number) {
    this.lista.items.splice(index, 1);
    this.deseosService.guardarStorage();
  }
}
