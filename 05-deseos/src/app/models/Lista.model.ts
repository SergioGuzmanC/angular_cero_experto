import { ListItems } from "./list-item.model";

export class Lista{

    id:number;
    titulo:string;
    creadoEn: Date;
    termiandoEn: Date;
    terminado:boolean;
    items: ListItems[];


    constructor(titulo:string){
        this.titulo = titulo;
        this.creadoEn = new Date();
        this.id = new Date().getTime();
        this.terminado = false;
        this.items = [];
    }
}