import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2'

import { AuthService } from 'src/app/services/auth.service';

import { UsuarioModel } from '../../models/usuario.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();;
  recordar:boolean = false;

  constructor(private authService: AuthService, private router:Router) { }

  ngOnInit() {

    if(localStorage.getItem('email')) {
      this.usuario.email =  localStorage.getItem('email');
      this.recordar = true;
    }

  }

  login(form: NgForm) {

    if (form.invalid) return;

    Swal.fire({
      allowOutsideClick:false,
      type: 'info',
      text: 'Espere por favor...'
    });

    Swal.showLoading();

    this.authService.login(this.usuario).subscribe(response => {
      console.log(response);
      Swal.close();
      if(this.recordar) localStorage.setItem('email', this.usuario.email);
      
      this.router.navigateByUrl('/home');
    }, (err) => {
      Swal.fire({
        type: 'error',
        title: 'Error al autenticar',
        text: err.error.error.message
      });
    });
  }

}
