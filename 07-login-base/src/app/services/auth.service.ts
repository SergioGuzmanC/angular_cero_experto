import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { UsuarioModel } from '../models/usuario.model';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url: string = 'https://identitytoolkit.googleapis.com/v1/accounts:';
  private apiKey: string = 'AIzaSyA4_rv3Mv67JF5Yn6LO1v5wa5LC0VpMJts';

  private signUp: String = 'signUp?key=';
  private signIn: string = 'signInWithPassword?key=';

  userToken: string;

  constructor(private http: HttpClient) {
    this.getToken();
  }

  logOut() {
    localStorage.removeItem('token');
  }

  login(user: UsuarioModel) {
    let authData = {
      ...user,
      returnSecureToken: true
    };
    return this.http.post(this.url + this.signIn + this.apiKey, authData)
      .pipe(
        map(response => {
          console.log('Si se habilito el mapa');
          this.guardarToken(response['idToken'])
          return response;
        }));

  }

  newUser(user: UsuarioModel) {
    let authData = {
      ...user,
      returnSecureToken: true
    };
    return this.http.post(this.url + this.signUp + this.apiKey, authData)
      .pipe(
        map(response => {
          this.guardarToken(response)
          return response;
        }));
  }


  private guardarToken(response) {
    this.userToken = response['idToken'];
    localStorage.setItem('token', this.userToken);
    let hoy = new Date();
    hoy.setSeconds(response['expiresIn']);
    localStorage.setItem('expira', hoy.getTime().toString())
  }

  getToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }
  }
  

  getAutetication(): boolean{
    if(this.userToken.length < 2){
      return false;
    }
    const expira = new Date();
    expira.setTime(Number(localStorage.getItem('expira')));

    if(expira > new Date()) return true;
    else return false;

  }

}