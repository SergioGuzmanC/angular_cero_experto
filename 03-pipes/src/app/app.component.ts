import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  nombre:string = 'Capitán América';
  nombre2:string = 'ErIk SeRgIo GuZmAn CoNtReRaS';
  arreglo:number[] = [1,2,3,4,5,6,7,8,9,10];

  PI:number = Math.PI;
  porcentaje:number = 0.234;
  videoUrl:string = 'https://www.youtube.com/embed/o_aMDoFs6vc';
  salario: number = 12345;

  activar:boolean = true;

  idioma:string = 'es';
  valorPromesa = new Promise<string>((resolve) =>{
    setTimeout(() =>{
      resolve('llego la data');
    }, 4500);
  });

  fecha:Date = new Date();
  heroe = {
    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 500,
    direccion:{
      calle: 'primera',
      casa:20
    }
  }

}
