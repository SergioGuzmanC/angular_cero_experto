import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `

    <p [style.fontSize.px]="tamano">
      Hola Mundo.... Está es una Etiqueta.....
    </p>
    <button class="btn btn-primary mr-1" (click)="tamano = tamano + 5">
      <i class="fas fa-plus"></i>
    </button>

    <button class="btn btn-primary ml-1" (click)="tamano = tamano - 5">
      <i class="fas fa-minus"></i>
    </button>

  `,
  styles: [
  ]
})
export class NgStyleComponent implements OnInit {

  constructor() { }

  tamano: number = 20;
  ngOnInit(): void {
  }

}
