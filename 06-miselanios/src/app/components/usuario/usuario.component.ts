import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {

id:string = '';

  constructor(private router:ActivatedRoute) {
    this.router.params.subscribe(parametros =>{
      console.log('RUTA PADRE');
      console.log(parametros);
      this.id = parametros['id'];
      console.log(this.id);
    })
  }

  ngOnInit(): void {
  }

}
