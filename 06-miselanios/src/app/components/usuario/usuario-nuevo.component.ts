import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-usuario-nuevo',
  template: `
    <p>
      usuario-nuevo works!
    </p>
  `,
  styles: [
  ]
})
export class UsuarioNuevoComponent implements OnInit {

id:string = '';

  constructor(private router: ActivatedRoute) {
    this.router.parent.params.subscribe(params =>{
      console.log('RUTA HIJA');
      console.log(params);
      this.id = params['id'];
      console.log(this.id);

    })
  }

  ngOnInit(): void {
  }

}
