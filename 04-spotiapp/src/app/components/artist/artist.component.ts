import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: [
  ]
})
export class ArtistComponent {

  artist:any = {};
  loading:boolean;
  toptracks:any[] = []; 

  constructor(private spotify:SpotifyService, private route:ActivatedRoute) { 
    this.loading = true;
    this.route.params.subscribe( params => this.getArtists( params['id'] ) );    
    this.route.params.subscribe( params => this.getTopTracks( params['id'] ) );
    this.loading = false;
  }

  getArtists(id:string){
    this.spotify.getArtist(id).subscribe( resp => this.artist = resp);
  }

  getTopTracks(id:string){
    this.spotify.getArtistsTopTracks(id).subscribe(resp => this.toptracks = resp);
  }

}
